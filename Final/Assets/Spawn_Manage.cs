﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn_Manage : MonoBehaviour {


    public int maxPlat = 20;
    public GameObject plat;
    public float horizontalMin = 6.5f;
    public float horizontalMax = 12f;
    public float verticalMin = -2f;
    public float verticalMax = 2f;

    private Vector2 originPos;

	// Use this for initialization
	void Start () {
        originPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        for (int i = 0; i < maxPlat; i++){
            Vector2 randomPosition = originPos + new Vector2(Random.Range(horizontalMin, horizontalMax), Random.Range(verticalMin, verticalMax));
            Instantiate(plat, randomPosition, Quaternion.identity);
            originPos = randomPosition;
        }
	}
}
