﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Move : MonoBehaviour {

    public int playerSpeed = 10;
    public bool facingRight = true;
    public int playerJump = 1250;
    public float moveX;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        PlayerMove();
	}
    void PlayerMove()
    {
        //CONTROL
        moveX = Input.GetAxis("Horizontal");
        if (Input.GetButtonDown("Jump"))
        {
            Jump();
        }
        //AMIMATIONS
        //PLAYER DIRECTIONS
        if(moveX < 0.0f && facingRight == false)
        {
            FlipPlayer();
        }
        else if(moveX > 0.0f && facingRight == true)
        {
            FlipPlayer();
        }
        //PHYSICS
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(moveX * playerSpeed, gameObject.GetComponent<Rigidbody2D>().velocity.y);
    }
    void Jump()
    {
        //Jumping code
        GetComponent<Rigidbody2D>().AddForce(Vector2.up * playerJump);
       
    }
    void FlipPlayer()
    {
        facingRight = !facingRight;
        Vector2 localScale = gameObject.transform.localScale;
        localScale.x *= -1;
        transform.localScale = localScale;
    }

    void OnTriggerEnter2D(Collider2D trig)
    {
        if (trig.gameObject.name == "Coin")
        {
            playerSpeed += 2;
            Destroy(trig.gameObject);
        }

        if (trig.gameObject.name == "Slow")
        {
            playerSpeed -= 5;
            Destroy(trig.gameObject);
        }

        if (trig.gameObject.name == "End"){
            playerSpeed = 0;
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("enemy")){
            if (collision.transform.position.y < transform.position.y - 1)
            {
                GetComponent<Rigidbody2D>().AddForce(Vector2.up * 1000);
                collision.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.right * 200);
                collision.gameObject.GetComponent<BoxCollider2D>().enabled = false;
                collision.gameObject.GetComponent<EnemyMovement>().enabled = false;
                print("hello");
            }
        }


    }
}
