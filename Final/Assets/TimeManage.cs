﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class TimeManage : MonoBehaviour
{
    public float gameTime;

    Text text;

    void Start()
    {
        text = GetComponent<Text>();
        gameTime = 100f;
    }

    // Update is called once per frame
    void Update()
    {
        text.text = "Time: " + Mathf.CeilToInt(gameTime) + " sec";
        gameTime = gameTime - Time.deltaTime;
        if (gameTime < 0.1f)
        {
            SceneManager.LoadScene("Prototype_1");
        }
    }
}